package dto;

public class operaciones {
	
	//Aqu� hago todas las operaciones que utilizaremos en el programa
	//suma
	public static double sumaNumeros(double primerNumeroDouble, double segundoNumeroDouble) {
		double suma = primerNumeroDouble + segundoNumeroDouble;
		
		return suma;
		
	}
	//resta
	public static double restaNumeros(double primerNumeroDouble, double segundoNumeroDouble) {
		
		double resta = primerNumeroDouble - segundoNumeroDouble;
		
		return resta;
		
	}
	//multiplicar
	public static double multiplicarNumeros(double primerNumeroDouble, double segundoNumeroDouble) {
		
		double multiplicar = primerNumeroDouble * segundoNumeroDouble;
		
		return multiplicar;
		
	}
	//dividir
	public static double dividirNumeros(double primerNumeroDouble, double segundoNumeroDouble) {
		
		double dividir = primerNumeroDouble / segundoNumeroDouble;
		
		return dividir;
		
	}
	//potencia
	public static double potenciaNumeros(double primerNumeroDouble, double segundoNumeroDouble) {
		
		Double potencia = (double) Math.pow(primerNumeroDouble, segundoNumeroDouble);
		
		return potencia;
		
	}
	//raizCuadrada
	public static double raizCuadradaNumeros(double primerNumeroDouble) {
		
		double raiz = (double) Math.sqrt(primerNumeroDouble);
		
		return raiz;
		
	}
	//raizCubica
	public static double raizCubicaNumeros(double primerNumeroDouble) {
		
		int raizCubica = (int) Math.cbrt(primerNumeroDouble);
		
		return raizCubica;
		
	}

}
