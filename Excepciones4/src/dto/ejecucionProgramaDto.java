package dto;

import javax.swing.JOptionPane;

import exceptions.excepcionNueva;
import views.viewsHija;

public class ejecucionProgramaDto {
	
	//Aqu� voy a crear un m�todo donde agrupar� todos los m�todos y los encapsular� en un solo m�todo
	//para posteriormente ejecutarlo al Main
	public static void ejecutarPrograma() throws excepcionNueva {
		
		String eleccion = JOptionPane.showInputDialog(null, "1. Sumar | 2. restar | 3. multiplicar | 4. dividir "
													+ "| 5. potencia | 6. ra�z cuadrada | 7. ra�z cubica");
		int eleccionInt = Integer.parseInt(eleccion);


		//Aqu� llama el m�todo del package views
		viewsHija.menu(eleccionInt);

    }

}