package views;

import javax.swing.JOptionPane;

import dto.operaciones;
import exceptions.excepcionNueva;

public class viewsHija {
	
	//Aqu� preguntamos al usuario que operaci�n matem�tica quiere hacer
	public static int pregunta() {
		
		String eleccion = JOptionPane.showInputDialog(null, "1. Sumar | 2. restar | 3. multiplicar | 4. dividir "
													+ "| 5. potencia | 6. ra�z cuadrada | 7. ra�z cubica");
		int eleccionInt = Integer.parseInt(eleccion);
		
		return eleccionInt;
		
}
	//Aqu� creo un m�todo donde ahy un switch case con 
	public static void menu(int eleccionInt) throws excepcionNueva {
		
		double numero1;
		double numero2;
		double resultadoDeOperacion = 0;
		
		switch (eleccionInt) {
		case 1:
			numero1 = viewsPadre.numeroUsuario();
			numero2 =  viewsPadre.numeroUsuario();
			resultadoDeOperacion = operaciones.sumaNumeros(numero1, numero2);
			break;
		case 2:
			numero1 = viewsPadre.numeroUsuario();
			numero2 =  viewsPadre.numeroUsuario();
			resultadoDeOperacion = operaciones.restaNumeros(numero1, numero2);
			break;
		case 3:
			numero1 = viewsPadre.numeroUsuario();
			numero2 =  viewsPadre.numeroUsuario();
			resultadoDeOperacion = operaciones.multiplicarNumeros(numero1, numero2);
			break;
		case 4:
			numero1 = viewsPadre.numeroUsuario();
			numero2 =  viewsPadre.numeroUsuario();
			try {
				if(numero1 == 0 || numero2 == 0) {
					throw new excepcionNueva("dividir");
				}else {
					resultadoDeOperacion = operaciones.dividirNumeros(numero1, numero2);
				}
			}catch (excepcionNueva sergi) {
				System.out.println(sergi.getMessage());
			}

			break;
		case 5:
			numero1 = viewsPadre.numeroUsuario();
			numero2 =  viewsPadre.numeroUsuario();
			resultadoDeOperacion = operaciones.potenciaNumeros(numero1, numero2);
			break;
		case 6:
			try {
				numero1 = viewsPadre.numeroUsuario();
				if(numero1 < 0) {
					throw new excepcionNueva("raizCuadrada");
				}else {
					resultadoDeOperacion = operaciones.raizCuadradaNumeros(numero1);
				}
			} catch (excepcionNueva sergi) {
				System.out.println(sergi.getMessage());
			}

			break;
		case 7:
			try {
				numero1 = viewsPadre.numeroUsuario();
				if(numero1 < 0) {
					throw new excepcionNueva("raizCubica");
				}else {
					resultadoDeOperacion = operaciones.raizCubicaNumeros(numero1);
				}
			}catch (excepcionNueva sergi) {
				System.out.println(sergi.getMessage());
			}

			break;
			//En el caso de que no se cumpla ninguno de los anteriores case se ejecutar� el default
			default:
				System.out.println("Introduce un n�mero v�lido");
				break;

		}
		//Aqu� mostramos por pantalla el 
		System.out.println(resultadoDeOperacion);
	}
	
}
