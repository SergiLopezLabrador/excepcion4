package exceptions;
import views.viewsPadre;

public class saberNumero {
	
	//Aqu� creo un m�todo donde identifico si el usuario escribe bien el dato que se le pide por Jpanel
	public static double identificarNumero() {
        double num = 0;

        try {
            num = viewsPadre.numeroUsuario();

        //En el caso de que el usuario no escriba lo que se le pide, se activar� el catch y 
        // saldr� el mensaje que est� dentro del catch
        }catch (NumberFormatException a) {
            System.out.println("No has introducido un n�mero valido");
        }

        return num;
    }

}