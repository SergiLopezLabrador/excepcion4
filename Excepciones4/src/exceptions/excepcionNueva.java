package exceptions;

public class excepcionNueva extends Exception{
    

	//Aqu� creamos la variable tipo private
    private String MensajeInformativo;
    
    //Aqu� creo el constructor por default
    public excepcionNueva() {
    	super();
    }
     
    //Aqu� creo un constructor donde le doy valor a la variable creada anteriormente
    public excepcionNueva(String MensajeInformativo){
    	super();
        this.MensajeInformativo=MensajeInformativo;
    }
    

     

    //Aqu� creo un m�todo donde recibo el c�digo de error de viewsHija
    public String getMessage(){
         
    	//Aqu� creo una variable con un valor definido, donde luego lo modificaremos dependiendo
    	//de que informaci�n nos envie viewsHija
        String Informacion="Prueba";
         
        //Aqu� creo un switch case donde, por cada mensaje de error que recibe, ejecuta la frase indicada abajo
        switch(MensajeInformativo){
            case "raizCuadrada":
            	Informacion="No puede ser inferior a 0";
                break;   
            case "raizCubica":
            	Informacion="No puede ser inferior a 0";
                break;   
            case "division":
            	Informacion="Ninguno de los n�meros puede ser 0";
                break;
        }
         

        return Informacion;
         
    }
     
}